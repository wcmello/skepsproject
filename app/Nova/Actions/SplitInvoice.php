<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Heading;
use App\Models\Invoice;
use Illuminate\Support\Facades\Http;

class SplitInvoice extends Action
{
    use InteractsWithQueue, Queueable;

    //makes it only visable on detail page of resource
    public $onlyOnDetail = true;
    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        if (is_numeric($fields->amount1) &&  is_numeric($fields->amount2)) {

        foreach ($models as $project) {
            if ($fields->amount1 + $fields->amount2 <= $project->budget) {
            foreach ($project->customers as $customer) {
                $parameters = [
                    'api_key'       => config('services.wefact'),
                    'controller'    => 'invoice',
                    'action'        => 'add',
                    "DebtorCode"    => $customer->debtorcode,
                    "InvoiceLines"  => [
                        [
                            "Description" => $project->description,
                            "PriceExcl" => $fields->amount1
                        ]                            

                    ]
                ];
                $response = Http::post('https://api.mijnwefact.nl/v2/', $parameters);

                Invoice::create(['project_id' => $project->id, 'invoicenr' => $response['invoice']['InvoiceCode']]);
                $parameters = [
                    'api_key'       => config('services.wefact'),
                    'controller'    => 'invoice',
                    'action'        => 'add',
                    "DebtorCode"    => $customer->debtorcode,
                    "InvoiceLines"  => [
                        [
                            "Description" => $project->description,
                            "PriceExcl" => $fields->amount2
                        ]                            

                    ]
                ];
                $response = Http::post('https://api.mijnwefact.nl/v2/', $parameters);
                Invoice::create(['project_id' => $project->id, 'invoicenr' => $response['invoice']['InvoiceCode']]);
            }

            }
            else
            {
                return Action::danger('The sum of the two invoices may not be greater than the original budget');
            }
        }

        }
        else
        {
            return Action::danger('Please fill in two numbers');
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Heading::make('Split the project into two invoices'),
           
                
                Currency::make('Amount1'),

                Currency::make('Amount2'),
            
        ];
    }
}
