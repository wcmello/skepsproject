<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Support\Facades\Http;


class ExportToWefact extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
       $count = 0;
       foreach ($models as $model) {
        if ($model->exported == 0) {
           $response = Http::post('https://api.mijnwefact.nl/v2/', [
                'api_key' => config('services.wefact'),
                'controller' => 'debtor',
                'action' => 'add',
                'CompanyName' => $model->name,
                'Address' => $model->street,
                'ZipCode' => $model->zip,
                'City' => $model->city,
                'EmailAddress' => $model->email,
                'PhoneNumber' => $model->phone,

            ]);
           $model->exported = 1;
           $model->debtorcode = $response['debtor']['DebtorCode'];
           $model->save();

           $count++;

        }
       }
       return Action::message($count . ' customer(s) added succesfully');

    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
