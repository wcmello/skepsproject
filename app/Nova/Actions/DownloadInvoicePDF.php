<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class DownloadInvoicePDF extends Action
{
    use InteractsWithQueue, Queueable;


    public $onlyOnDetail = true;
    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $invoice) {
            $parameters = [
                'api_key' => config('services.wefact'),
                'controller' => 'invoice',
                'action' => 'download',
                'InvoiceCode' => $invoice->invoicenr
            ];
            $response = Http::post('https://api.mijnwefact.nl/v2/', $parameters);

           /* $path = public_path('/files/' . $response['invoice']['Filename']);*/
            $pdf = base64_decode($response['invoice']['Base64']);

            /*file_put_contents($path, $pdf);*/
            Storage::disk('public')->put($response['invoice']['Filename'], $pdf);
            $url = Storage::url($response['invoice']['Filename']);

            return Action::download(url($url), $response['invoice']['Filename']);
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
