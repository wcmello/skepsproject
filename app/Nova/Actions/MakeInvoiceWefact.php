<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Support\Facades\Http;
use App\Models\Invoice;

class MakeInvoiceWefact extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach($models as $model){
            foreach ($model->customers as $customer) {
                if (!Invoice::where('project_id','=', $model->id)->first()) {
                    $parameters = [
                        'api_key'       => config('services.wefact'),
                        'controller'    => 'invoice',
                        'action'        => 'add',
                        "DebtorCode"    => $customer->debtorcode,
                        "InvoiceLines"  => [
                            [
                                "Description" => $model->description,
                                "PriceExcl" => $model->budget
                            ]
                        ]
                    ];
                    $response = Http::post('https://api.mijnwefact.nl/v2/', $parameters);

                    Invoice::create(['project_id' => $model->id, 'invoicenr' => $response['invoice']['InvoiceCode']]);
                }  
            }
            
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
