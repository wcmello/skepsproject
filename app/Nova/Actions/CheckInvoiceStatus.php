<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Support\Facades\Http;

class CheckInvoiceStatus extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $invoice) {
            $parameters = [
                    'api_key' => config('services.wefact'),
                    'controller' => 'invoice',
                    'action' => 'show',
                    'InvoiceCode' => $invoice->invoicenr
                ];
                $response = Http::post('https://api.mijnwefact.nl/v2/', $parameters);

            switch ($response['invoice']['Status']) {
                case 0:
                    $invoice->status = 'Concept Factuur';
                    break;
                case 2:
                    $invoice->status = 'Verzonden';
                    break;
                case 3:
                    $invoice->status = 'Deels betaald';
                    break;
                case 4:
                    $invoice->status = 'Betaald';
                    break;
                case 8:
                    $invoice->status= 'Creditfactuur';
                    break;
                case 9:
                    $invoice->status = 'Vervallen';
                    break;
                default:
                    $invoice->status = 'N/A';
                    break;
            }
            $invoice->save();

            

        }
        return Action::message('Invoice status updated');
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
