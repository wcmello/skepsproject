<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'description',
        'budget',
        'status_id',
    ];

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice');
    }
   	public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }
    public function customers()
    {
        return $this->belongsToMany('App\Models\Customer');
    }

 
}
