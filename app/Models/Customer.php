<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'street',
        'zipcode',
        'city',
        'phone',
        'exported',
    ];
    public function projects()
    {
        return $this->belongsToMany('App\Models\Project');
    }
}
